<?php
    require_once "DB_Class.php";

    class Index extends DataBase{
        private $order="DESC";//тип сортировки по умолчанию
        private $order_by="date";//сортировка отзывов по умолчанию
        private $max_width=320;
        private $max_height=240;

        public function __construct()
        {
            $this->db_prefix=parent::getTablePrefix();
            $this->db_connect=parent::getDB();
            $this->img_uploaded_path=parent::getPath();
            $this->default_img_url=parent::getDefaultUrl();
        }

        /*
            Метод получения отзывов из БД.
            Принимает тип сортировки.
            Возвращает массив.
        */
        public function getReviews($sort_type)
        {
            switch($sort_type){
                case "name":
                    $query = "
                        SELECT * FROM `".$this->db_prefix."reviews` WHERE `status`='1' ORDER BY `name` ASC
                    ";
                    $returnable_array = $this->db_connect->select($query);
                break;
                case "email":
                    $query = "
                        SELECT * FROM `".$this->db_prefix."reviews` WHERE `status`='1' ORDER BY `email` ASC
                    ";
                    $returnable_array = $this->db_connect->select($query);
                break;
                default:
                    $query = "
                        SELECT * FROM `".$this->db_prefix."reviews` WHERE `status`='1' ORDER BY `{?}` {?}
                    ";
                    $returnable_array = $this->db_connect->select($query, array($this->order_by, $this->order));
                break;
            }

            return $returnable_array;
        }

        /*
            Метод конвертации unix время в человеко-понятное.
            Принимает формат даты, unix время.
            Возвращает строку-дату в заданном формате.
        */
        public function getTime($data_format, $unix_time)
        {
            $rezult=date($data_format, $unix_time);
            return $rezult;
        }

        /*
            Метод получения пути к папке загружаемых изображения.
            Возвращает строку-путь.
        */
        public function getImgPatch()
        {
            return $this->img_uploaded_path;
        }

        /*
            Метод добавления отзыва в БД.
            Принимает имя, емейл, текст, урл картинки(не обязательно).
            Возвращант последний вставленный id в случае успеха,
            иначе false.
        */
        public function addOneReview($name, $email, $text, $img_src)
        {
            $name=$this->realEscapeString($name);
            $email=$this->realEscapeString($email);
            $text=$this->realEscapeString($text);

            $name=strip_tags($name);
            $email=strip_tags($email);
            $text=strip_tags($text);
            
            $date=time();

            $query="
                INSERT INTO `".$this->db_prefix."reviews` (
                    `name`,
                    `email`,
                    `text`,
                    `status`,
                    `date`,
                    `is_edited`,
                    `img_url`
                )
                VALUES (
                    '$name',
                    '$email',
                    '$text',
                    '0',
                    '$date',
                    '0',
                    ";
                    if($img_src===false){
                        $query.="'$this->default_img_url'";
                    } else{
                        $query.="'$img_src'";
                    }

                 $query.="
                )
            ";

            return $this->db_connect->query($query);
        }

        /*
            Метод экранирования ненужных символов для БД.
            Принимает строку.
            Возвращает строку-экранированный текст.
        */
        private function realEscapeString($string)
        {
            return addslashes($string);
        }

        /*
            Метод проверки файла на размер и MIME-type.
            Принимает темп-имя, полный путь к папке и картинке,
            максимальный размер в байтах, имя файла со стороны клиента,
            MIME-type, реальный размер файла в байтах.
            Возвращает true в случае успеха, иначе false.
        */
        public function uploadImg($tmp_name, $uploadfile, $max_img_size, $img_name, $file_type, $real_img_size)
        {
            if (($file_type != "image/jpg") && ($file_type  != "image/jpeg") && ($file_type  != "image/png") && ($file_type  != "image/gif")) return false;
            if ($real_img_size > $max_img_size) return false;

            if($this->get_image_type($tmp_name)===false) return false;

            if(move_uploaded_file($tmp_name, $uploadfile)){
                $img_width = $this->getImageWidth($uploadfile);//получаем ширину изображения
                $img_height = $this->getImageHeight($uploadfile);//получаем высоту изображения

                if($img_width>$this->max_width || $img_height>$this->max_height){//если размеры картинки больше указанных-пережимаем
                    $this->imageResize($uploadfile, $uploadfile, $this->max_width, $this->max_height);
                }
                return true;
            }

        }

        /*
            Метод дополнительной проверки файлов по сигнатуре.
            Принимает tmp файл.
            Возвращает тип данных, в случае успеха, иначе false.
        */
        private function get_image_type($file)
        {
            if (!$f = fopen($file, 'rb')) {
                return false;
            }

            $data = fread($f, 8);
            fclose($f);

            if (
                @array_pop(unpack('H12', $data)) == '474946383961' ||
                @array_pop(unpack('H12', $data)) == '474946383761'
            ) {
                return 'GIF';
            } else if (
                @array_pop(unpack('H4', $data)) == 'ffd8'
            ) {
                return 'JPEG';
            } else if (
                @array_pop(unpack('H16', $data)) == '89504e470d0a1a0a'
            ) {
                return 'PNG';
            }

            return false;
        }

        /*
            Метод получения ширины изображения.
            Принимает ссылку на файл.
            Возвращает число-ширину.
        */
        private function getImageWidth($file)
        {
            $arr=getimagesize($file);
            return $width=$arr[0];
        }

        /*
            Метод получения высоты изображения.
            Принимает ссылку на файл.
            Возвращает число-высоту.
        */
        private function getImageHeight($file)
        {
            $arr=getimagesize($file);
            return $height=$arr[1];
        }

        /*
            Метод пережатия картинки.
            Принимает $src - имя исходного файла,
            $dest - имя генерируемого файла,
            $width, $height - ширина и высота генерируемого изображения, в пикселях.
            Возвращает true в случае успеха,
            иначе-false.
        */
        public function imageResize($src, $dest, $width, $height)
        {
            $rgb=0xFFFFFF;
            $quality=100;
            if (!file_exists($src)) return false;

            $size = getimagesize($src);

            if ($size === false) return false;

            $format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
            $icfunc = "imagecreatefrom" . $format;
            if (!function_exists($icfunc)) return false;

            $x_ratio = $width / $size[0];
            $y_ratio = $height / $size[1];

            $ratio       = min($x_ratio, $y_ratio);
            $use_x_ratio = ($x_ratio == $ratio);

            $new_width   = $use_x_ratio  ? $width  : floor($size[0] * $ratio);
            $new_height  = !$use_x_ratio ? $height : floor($size[1] * $ratio);
            $new_left    = $use_x_ratio  ? 0 : floor(($width - $new_width) / 2);
            $new_top     = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);

            $isrc = $icfunc($src);
            $idest = imagecreatetruecolor($width, $height);

            imagefill($idest, 0, 0, $rgb);
            imagecopyresampled($idest, $isrc, $new_left, $new_top, 0, 0,
            $new_width, $new_height, $size[0], $size[1]);

            imagejpeg($idest, $dest, $quality);

            imagedestroy($isrc);
            imagedestroy($idest);

            return true;
        }

    }
?>
