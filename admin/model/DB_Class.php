<?php
    class DataBase {
        protected $table_prefix;
        protected $img_path;
        protected $config_path="../../config/config.php";

        private static $db = null; // Единственный экземпляр класса, чтобы не создавать множество подключений
        public $mysqli; // Идентификатор соединения
        private $sym_query = "{?}"; // "Символ значения в запросе"

        /*
            Получение экземпляра класса.
            Если он уже существует, то возвращается,
            если его не было, то создаётся и возвращается (паттерн Singleton)
        */
        public static function getDB()
        {
            if (self::$db == null) self::$db = new DataBase();
            return self::$db;
        }

        /*
            Метод получения папки сайта.
            Возвращает строку.
        */
        private function gerCurrentDomain()
        {
            $curr_adress=$_SERVER['DOCUMENT_ROOT'];
            return $curr_adress;
        }

        /*
            private-конструктор, подключающийся к базе данных,
            устанавливающий локаль и кодировку соединения
        */
        private function __construct()
        {
            $this->config = require $this->config_path;

            $this->db_name=$this->config["db_config"]["db_name"];
            $this->db_host=$this->config["db_config"]["db_host"];
            $this->db_user=$this->config["db_config"]["db_user"];
            $this->db_password=$this->config["db_config"]["db_password"];


            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
            $this->mysqli = new mysqli(
                $this->db_host,//хост
                $this->db_user,//пользователь
                $this->db_password,//пароль
                $this->db_name//имя базы
            );
            $this->mysqli->query("SET lc_time_names = 'ru_RU'");
            $this->mysqli->query("SET NAMES 'utf8'");

        }

        /*
            Метод возвращения префикса таблиц из
            конфига.
            Возвращает строку.
        */
        protected function getTablePrefix()
        {
             $conf=require $this->config_path;
             $table_prefix=$conf["db_config"]["table_prefix"];
             return $table_prefix;
        }

        /*
            Метод получения пути к изображениям.
            Возвращает строку.
        */
        protected function getPath()
        {
             $conf=require $this->config_path;
             $img_path=$conf["img"]["uploaded_path"];
             return $img_path;
        }

        /*
            Метод получения пути к дефолтному изображению.
            Возвращает строку.
        */
        protected function getDefaultUrl()
        {
            $conf=require $this->config_path;
            $default_img_path=$conf["img"]["default_img_path"];
            return $default_img_path;
        }

        /*
            Вспомогательный метод, который заменяет "символ значения в запросе" на конкретное значение,
            которое проходит через "функции безопасности"
        */
        public function getQuery($query, $params)
        {
            if ($params) {
                for ($i = 0; $i < count($params); $i++) {
                    $pos = strpos($query, $this->sym_query);
                    $arg = "".$this->mysqli->real_escape_string($params[$i])."";
                    $query = substr_replace($query, $arg, $pos, strlen($this->sym_query));
                }
            }
            
          return $query;
        }

        /*
            SELECT-метод, возвращающий таблицу результатов
        */
        public function select($query, $params = false)
        {
            $result_set = $this->mysqli->query($this->getQuery($query, $params));
            if (!$result_set) return false;
            return $this->resultSetToArray($result_set);
        }

        /*
            SELECT-метод, возвращающий одну строку с результатом
        */
        public function selectRow($query, $params = false)
        {
            $result_set = $this->mysqli->query($this->getQuery($query, $params));
            if ($result_set->num_rows != 1) return false;
                else return $result_set->fetch_assoc();
        }

        /*
            SELECT-метод, возвращающий значение из конкретной ячейки
        */
        public function selectCell($query, $params = false)
        {
            $result_set = $this->mysqli->query($this->getQuery($query, $params));
            if ((!$result_set) || ($result_set->num_rows != 1)) return false;
            else {
                $arr = array_values($result_set->fetch_assoc());
                return $arr[0];
            }
        }

        /*
            НЕ-SELECT методы (INSERT, UPDATE, DELETE). Если запрос INSERT,
            то возвращается id последней вставленной записи
        */
        public function query($query, $params = false)
        {
            $success = $this->mysqli->query($this->getQuery($query, $params));
            if ($success) {
                if ($this->mysqli->insert_id === 0) return true;
                else return $this->mysqli->insert_id;
            }
            else return false;
        }

        /*
            Преобразование result_set в двумерный массив
        */
        private function resultSetToArray($result_set)
        {
            $array = array();
            while (($row = $result_set->fetch_assoc()) != false) {
                $array[] = $row;
            }
            return $array;
        }

        /*
            При уничтожении объекта закрывается соединение с базой данных
        */
        public function __destruct()
        {
            if ($this->mysqli) $this->mysqli->close();
        }
    }
?>