<?php
    require_once "DB_Class.php";

    class Edit extends DataBase{
        private $protocol="http://";//протокол
        private $current_domain;

        public function __construct()
        {
            $this->db_prefix=parent::getTablePrefix();
            $this->db_connect=parent::getDB();
            $this->current_domain=$_SERVER['SERVER_NAME'];
        }

        /*
            Метод получения информации о отзыве.
            Принимает id отзыва.
            Возвращает массив.
        */
        public function getOneReview($review_id)
        {
            if($this->isReview($review_id)===true){
                $query="
                     SELECT * FROM `".$this->db_prefix."reviews` WHERE `rv_id`='".$review_id."'
                ";
                $returnable_array = $this->db_connect->selectRow($query);
                return $returnable_array;
            }
            else return false;
        }

        /*
            Вспомогательный метод проверки строки на число.
            Принимает строку.
            Возвращает результат(true/false).
        */
        private function isNumber($string){
            $rezult=preg_match("/^\d{1,}$/", $string);
            return $rezult;
        }

        /*
            Метод проверки на существование отзыва.
            Принимает id отзыва.
            Возвращает true в случае успеха. Иначе-false.
        */
        private function isReview($review_id){
            $is_number=false;
            $is_number=$this->isNumber($review_id);

            $query = "
                SELECT `rv_id` FROM `".$this->db_prefix."reviews` WHERE `rv_id`='".$review_id."'
            ";
            $returnable_array = $this->db_connect->selectRow($query);
            if(($is_number==true) and (isset($returnable_array["rv_id"]))) return true;
            else return false;

        }

        /*
            Метод замены относительного пути изображения на абсолютный.
            Принимает путь вида ../view...
            Возвращает путь вида http://__сайт.__зона__/view...
        */
        public function getNormalImgPath($cut_img_path)
        {
            $normal_path=str_replace("../", $this->protocol.$this->current_domain."/", $cut_img_path);
            return $normal_path;
        }

        /*
            Метод редактирования отзыва.
            Принимает id отзыва, имя, email, текст, состояние, статус, дату.
            Возвращает true в случае успеха.
            Иначе-false.
        */
        public function updateReview($post_review_id, $new_name, $new_email, $new_text, $is_edited, $status, $date)
        {
            //экранируем слеши
            $new_name=$this->realEscapeString($new_name);
            $new_email=$this->realEscapeString($new_email);
            $new_text=$this->realEscapeString($new_text);
            $date=$this->getUnixDate($date);

            //убираем html
            $new_name=strip_tags($new_name);
            $new_email=strip_tags($new_email);
            $new_text=strip_tags($new_text);

            $query="
                UPDATE `".$this->db_prefix."reviews` SET
                    `name`='".$new_name."',
                    `email`='".$new_email."',
                    `text`='".$new_text."',
                    `date`='".$date."',

            ";

                    if($is_edited=="on"){
                        $query.="
                            `is_edited`='1',
                        ";
                    } else{
                        $query.="
                            `is_edited`='0',
                        ";
                    }
                    if($status=="reject"){
                        $query.="
                            `status`='0'
                        ";
                    } else{
                        $query.="
                            `status`='1'
                        ";
                    }

                $query.="
                    WHERE `rv_id`='".$post_review_id."'
                ";
            $rz=$this->db_connect->query($query);
            return $rz;
        }

        /*
            Метод экранирования ненужных символов для БД.
            Принимает строку.
            Возвращает строку-экранированный текст.
        */
        private function realEscapeString($string)
        {
            return addslashes($string);
        }

        /*
            Метод перевода строки в UNIX время.
            Принимает строку.
            Возвращает UNIX время.
        */
        private function getUnixDate($date)
        {
            $date=strtotime($date);
            return $date;
        }

        /*
            Метод конвертации unix время в человеко-понятное.
            Принимает формат даты, unix время.
            Возвращает строку-дату в заданном формате.
        */
        public function getTime($data_format, $unix_time)
        {
            $rezult=date($data_format, $unix_time);
            return $rezult;
        }
    }
?>