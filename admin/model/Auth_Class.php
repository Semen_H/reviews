<?php
    require_once "DB_Class.php";

    class Auth extends DataBase{
        private $db;
        private static $user=null;

        private function __construct(){
            $this->db_prefix=parent::getTablePrefix();
            $this->db_connect=parent::getDB();
        }

        public static function getObject(){
            if(self::$user===null) self::$user=new Auth();
            return self::$user;
        }

        /*
            Метод проверки сущесвтования логина и пароля.
            Принимает логин/пароль.
            Возвращает пароль пользователя в случае успеха.
            Иначе false.
        */
        private function checkUser($login, $password){
            $prefix=$this->db_prefix;
            $user=$this->db_connect->select("
                SELECT `password` FROM `".$prefix."users` WHERE `login`='$login'
            ");

            if(!$user) return false;
            return $user[0]["password"]===$password;
        }

        /*
            Метод проверки входа пользователя.
            Возвращает true в случае успеха.
        */
        public function isAuth(){
            session_start();
            $login=$_SESSION["login"];
            $password=$_SESSION["password"];
            return $this->checkUser($login, $password);
        }

        /*
            Метод авторизации.
            Принимает логин/пароль.
            Возвращает true в случае успеха.
            Иначе false.
        */
        public function login($login, $password){
            $password=md5($password);
            if($this->checkUser($login, $password)){
                session_start();
                $_SESSION["login"]=$login;
                $_SESSION["password"]=$password;
                return true;
            }
            else return false;
        }
    }
?>