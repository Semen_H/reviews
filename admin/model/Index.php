<?php
    require_once "DB_Class.php";

    class Index extends DataBase{
        private $order="DESC";//тип сортировки по умолчанию
        private $order_by="date";//сортировка отзывов по умолчанию
        private $protocol="http://";//протокол
        private $current_domain;

        public function __construct()
        {
            $this->db_prefix=parent::getTablePrefix();
            $this->db_connect=parent::getDB();
            $this->current_domain=$_SERVER['SERVER_NAME'];
        }

        /*
            Метод получения отзывов из БД.
            Принимает тип сортировки.
            Возвращает двумерный массив.
        */
        public function getReviews($sort_type)
        {
            switch($sort_type){
                case "name":
                    $query = "
                        SELECT * FROM `".$this->db_prefix."reviews` ORDER BY `name` ASC
                    ";
                    $returnable_array = $this->db_connect->select($query);
                break;
                case "email":
                    $query = "
                        SELECT * FROM `".$this->db_prefix."reviews` ORDER BY `email` ASC
                    ";
                    $returnable_array = $this->db_connect->select($query);
                break;

                case "accepted": //принятые
                    $query = "
                        SELECT * FROM `".$this->db_prefix."reviews` WHERE `status`='1' ORDER BY `{?}` ASC
                    ";
                    $returnable_array = $this->db_connect->select($query, array($this->order_by));
                break;
                case "rejected": //отклоненные
                    $query = "
                        SELECT * FROM `".$this->db_prefix."reviews` WHERE `status`='0' ORDER BY `{?}` ASC
                    ";
                    $returnable_array = $this->db_connect->select($query, array($this->order_by));
                break;
                case "edited": //измененные
                    $query = "
                        SELECT * FROM `".$this->db_prefix."reviews` WHERE `is_edited`='1' ORDER BY `{?}` ASC
                    ";
                    $returnable_array = $this->db_connect->select($query, array($this->order_by));
                break;

                default:
                    $query = "
                        SELECT * FROM `".$this->db_prefix."reviews` ORDER BY `{?}` {?}
                    ";
                    $returnable_array = $this->db_connect->select($query, array($this->order_by, $this->order));
                break;
            }

            return $returnable_array;
        }

        /*
            Метод экранирования ненужных символов для БД.
            Принимает строку.
            Возвращает строку-экранированный текст.
        */
        private function realEscapeString($string)
        {
            return addslashes($string);
        }

        /*
            Метод конвертации unix время в человеко-понятное.
            Принимает формат даты, unix время.
            Возвращает строку-дату в заданном формате.
        */
        public function getTime($data_format, $unix_time)
        {
            $rezult=date($data_format, $unix_time);
            return $rezult;
        }


        /*
            Метод замены относительного пути изображения на абсолютный.
            Принимает путь вида ../view...
            Возвращает путь вида http://__сайт.__зона__/view...
        */
        public function getNormalImgPath($cut_img_path)
        {
            $normal_path=str_replace("../", $this->protocol.$this->current_domain."/", $cut_img_path);
            return $normal_path;
        }


    }
?>