$(document).ready(function(){
   $('.disabled').on("click", function(){
       return false;
   });


   $("#edit_review").validate({

       rules:{

            name:{
                required: true,
                minlength: 4,
                maxlength: 20,
            },

            email:{
                required: true,
                email:true,
            },

            text:{
                required: true,
                minlength: 20
            },

            date:{
                required: true,
            }


       },

       messages:{

            name:{
                required: "Введите имя(обязательно)!",
                minlength: "Имя должно быть минимум 4 символа",
                maxlength: "Максимальное число символов - 20",
            },

            email:{
                required: "Введите email(обязательно)!",
                email: "Некорректный email адрес!",
            },

            text:{
                required: "Введите текст отзыва(обязательно)!",
                minlength: "Минимальное число символов - 20"
            },

            date:{
                required: "Введите дату(обязательно)",
            }

       },
    });
});