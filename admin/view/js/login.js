$(document).ready(function(){

   $("#login_form").validate({

       rules:{

            login:{
                required: true,
                minlength: 3,
                maxlength: 40,
            },

            password:{
                required: true,
            }

       },

       messages:{
            login:{
                required: "Введите логин(обязательно)!",
                minlength: "Логин должен быть минимум 3 символа",
                maxlength: "Максимальное число символов - 40",
            },

            password:{
                required: "Введите пароль(обязательно)!",
            }
       },



    });

});