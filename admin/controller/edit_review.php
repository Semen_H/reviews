<?php
    require_once "../model/Auth_Class.php";
    $user=Auth::getObject();
    $auth=$user->isAuth();

    if($auth){
        require_once "../model/Edit.php";
        $edit_obj=new Edit();
        $footer=file_get_contents( "../../incl/footer.php");//получаем футер
        $review_id=$_GET["review_id"];//получаем GET-запрос
        $username=$_SESSION["login"]; //логин юзера
        $review_info=$edit_obj->getOneReview($review_id);//получаем массив с данными о отзыве

        if($_POST){
            $post_review_id=$_POST["review_id"];//получаем реальный id отзыва
            $new_name=$_POST["name"];
            $new_email=$_POST["email"];
            $new_text=$_POST["text"];
            $is_edited=$_POST["edited"];
            $status=$_POST["inlineRadioOptions"];
            $date=$_POST["date"];

            if($new_name && $new_email && $new_text){
                $rezult_upd=$edit_obj->updateReview($post_review_id, $new_name, $new_email, $new_text, $is_edited, $status, $date);
            }
            if($rezult_upd===true){
                header("Location: ../controller/index.php?message=success_update");
            }
        }


    require_once "../view/edit.html";
    }
    else {
         header("Location: login.php");
    }
?>