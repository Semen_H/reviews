<?php
    require_once "../model/Login.php";
    require_once "../model/Auth_Class.php";

    $user1=Auth::getObject();
    $auth=$user1->isAuth();
    $login_obj=new Login();
    $msg_error=false;

    if(isset($_POST["submit"])){

        $login=trim($_POST["login"]);
        $password=trim($_POST["password"]);
        $auth_success=$user1->login($login, $password);
        if($auth_success){
            header("Location: index.php");
            exit;
        }
        else $msg_error=true;

    }

    require_once "../view/login.html";
?>