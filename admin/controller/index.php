<?php
    require_once "../model/Auth_Class.php";
    $user=Auth::getObject();
    $auth=$user->isAuth();

    if($auth){
        require_once "../model/Index.php";
        $index_obj=new Index();
        $footer=file_get_contents( "../../incl/footer.php");
        $username=$_SESSION["login"];
        $sort=$_GET["sort"];
        $message=$_GET["message"];

        /***********************
            Сортировка
        ***********************/
        if($sort){
            switch($sort){
                case "name":
                    $rev_array=$index_obj->getReviews($sort_type="name"); //получаем массив с отзывами из бд
                break;
                case "email":
                    $rev_array=$index_obj->getReviews($sort_type="email");
                break;
                case "accepted":
                    $rev_array=$index_obj->getReviews($sort_type="accepted");
                break;
                case "rejected":
                    $rev_array=$index_obj->getReviews($sort_type="rejected");
                break;
                case "edited":
                    $rev_array=$index_obj->getReviews($sort_type="edited");
                break;
                default:
                    $rev_array=$index_obj->getReviews($sort_type="default");
                break;
            }
        }
        else{
            $rev_array=$index_obj->getReviews($sort_type="default"); 
        }

        require_once "../view/index.html";
    } else {
         header("Location: login.php");
    }
?>