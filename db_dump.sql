-- phpMyAdmin SQL Dump
-- version 4.0.10.12
-- http://www.phpmyadmin.net
--
-- Хост: semen06.mysql.ukraine.com.ua
-- Время создания: Сен 27 2016 г., 20:26
-- Версия сервера: 5.6.27-75.0-log
-- Версия PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `semen06_beejee`
--

-- --------------------------------------------------------

--
-- Структура таблицы `bj_reviews`
--

CREATE TABLE IF NOT EXISTS `bj_reviews` (
  `rv_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `img_url` varchar(255) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `date` int(11) unsigned NOT NULL,
  `is_edited` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`rv_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=118 ;

--
-- Дамп данных таблицы `bj_reviews`
--

INSERT INTO `bj_reviews` (`rv_id`, `name`, `email`, `text`, `img_url`, `status`, `date`, `is_edited`) VALUES
(117, 'Семен', 'semenius5@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec velit mauris, ornare vitae sem pulvinar, volutpat commodo sapien. Cras placerat quam sit amet molestie congue. Quisque cursus, dolor et molestie efficitur, mauris nisi suscipit sem, eget feugiat purus mauris eget magna. Donec ut eros sed metus ornare tincidunt ultricies maximus tellus. Donec ac risus eget enim dictum suscipit non in dolor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc sollicitudin egestas augue ac aliquet. Morbi vulputate pretium libero vitae varius.', '../view/img/uploaded/IMG_20160924_105552.jpg', 1, 1474988977, 1),
(114, 'Тестовый отзыв', 'semenius5@gmail.com', 'dfdfdfdТекст сообщения', '../view/img/uploaded/hs-2013-12-a-print.jpg', 1, 1474243200, 1),
(112, 'Семен', 'semenius5@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vitae est euismod, imperdiet nisl id, vestibulum est. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Aenean venenatis suscipit efficitur. In faucibus fermentum porttitor. Nam hendrerit consectetur iaculis. Pellentesque auctor bibendum augue, ac lobortis sapien accumsan in. Donec at lacus condimentum, placerat ante quis, eleifend ipsum. Cras non velit vel quam vestibulum bibendum quis non odio. Sed non gravida dui, eget fermentum eros. Nullam eu nunc dolor. Donec posuere pretium ligula a mattis. Suspendisse id nisl metus. Proin odio massa, consectetur sit amet malesuada eu, bibendum in libero. Nunc finibus leo vitae felis tristique, et ultrices dui euismod.', 'http://beejee.semen.in.ua/view/img/default.jpg', 1, 1474294322, 0),
(113, 'Тестовый отзыв', 'semenius5@gmail.com', '19.09.2016 17:12:2519.09.2016 17:12:25', 'http://beejee.semen.in.ua/view/img/default.jpg', 0, 1474294345, 1),
(111, 'Герман', 'svg@dd.ua', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vitae est euismod, imperdiet nisl id, vestibulum est. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Aenean venenatis suscipit efficitur. In faucibus fermentum porttitor. Nam hendrerit consectetur iaculis. Pellentesque auctor bibendum augue, ac lobortis sapien accumsan in. Donec at lacus condimentum, placerat ante quis, eleifend ipsum. Cras non velit vel quam vestibulum bibendum quis non odio. Sed non gravida dui, eget fermentum eros. Nullam eu nunc dolor. Donec posuere pretium ligula a mattis. Suspendisse id nisl metus. Proin odio massa, consectetur sit amet malesuada eu, bibendum in libero. Nunc finibus leo vitae felis tristique, et ultrices dui euismod.', '../view/img/uploaded/image_resized.png', 1, 1474294293, 0),
(110, 'Вячеслав', 'va@mail.ru', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vitae est euismod, imperdiet nisl id, vestibulum est. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Aenean venenatis suscipit efficitur. In faucibus fermentum porttitor. Nam hendrerit consectetur iaculis. Pellentesque auctor bibendum augue, ac lobortis sapien accumsan in. Donec at lacus condimentum, placerat ante quis, eleifend ipsum. Cras non velit vel quam vestibulum bibendum quis non odio. Sed non gravida dui, eget fermentum eros. Nullam eu nunc dolor. Donec posuere pretium ligula a mattis. Suspendisse id nisl metus. Proin odio massa, consectetur sit amet malesuada eu, bibendum in libero. Nunc finibus leo vitae felis tristique, et ultrices dui euismod.', '../view/img/uploaded/doc157267238_187658749.gif', 1, 1474294199, 1),
(115, 'Семен', 'semenius5@gmail.com', 'Тестовый отзывТестовый отзыв', 'http://beejee.semen.in.ua/view/img/default.jpg', 0, 1474918061, 0),
(116, 'Семен', 'semenius5@gmail.com', 'Тестовый отзывТестовый отзыв', 'http://beejee.semen.in.ua/view/img/default.jpg', 0, 760917600, 1),
(109, 'Богдан', 'bg@bk.ru', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vitae est euismod, imperdiet nisl id, vestibulum est. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Aenean venenatis suscipit efficitur. In faucibus fermentum porttitor. Nam hendrerit consectetur iaculis. Pellentesque auctor bibendum augue, ac lobortis sapien accumsan in. Donec at lacus condimentum, placerat ante quis, eleifend ipsum. Cras non velit vel quam vestibulum bibendum quis non odio. Sed non gravida dui, eget fermentum eros. Nullam eu nunc dolor. Donec posuere pretium ligula a mattis. Suspendisse id nisl metus. Proin odio massa, consectetur sit amet malesuada eu, bibendum in libero. Nunc finibus leo vitae felis tristique, et ultrices dui euismod.', '../view/img/uploaded/doc171314236_181309754.gif', 1, 1474294130, 1),
(108, 'Алексей', 'a@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vitae est euismod, imperdiet nisl id, vestibulum est. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Aenean venenatis suscipit efficitur. In faucibus fermentum porttitor. Nam hendrerit consectetur iaculis. Pellentesque auctor bibendum augue, ac lobortis sapien accumsan in. Donec at lacus condimentum, placerat ante quis, eleifend ipsum. Cras non velit vel quam vestibulum bibendum quis non odio. Sed non gravida dui, eget fermentum eros. Nullam eu nunc dolor. Donec posuere pretium ligula a mattis. Suspendisse id nisl metus. Proin odio massa, consectetur sit amet malesuada eu, bibendum in libero. Nunc finibus leo vitae felis tristique, et ultrices dui euismod.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vitae est euismod, imperdiet nisl id, vestibulum est. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Aenean venenatis suscipit efficitur. In faucibus fermentum porttitor. Nam hendrerit consectetur iaculis. Pellentesque auctor bibendum augue, ac lobortis sapien accumsan in. Donec at lacus condimentum, placerat ante quis, eleifend ipsum. Cras non velit vel quam vestibulum bibendum quis non odio. Sed non gravida dui, eget fermentum eros. Nullam eu nunc dolor. Donec posuere pretium ligula a mattis. Suspendisse id nisl metus. Proin odio massa, consectetur sit amet malesuada eu, bibendum in libero. Nunc finibus leo vitae felis tristique, et ultrices dui euismod.', '../view/img/uploaded/2.jpg', 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `bj_users`
--

CREATE TABLE IF NOT EXISTS `bj_users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `bj_users`
--

INSERT INTO `bj_users` (`user_id`, `login`, `password`) VALUES
(1, 'admin', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
